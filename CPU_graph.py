import time
import os, platform, multiprocessing, subprocess
from collections import OrderedDict
import matplotlib.pyplot as plt
import numpy as np

data = OrderedDict()

data['OS_NAME'] = platform.system()
data['OS_VERSION'] = platform.release()

data['CPU_NAME'] = platform.processor()
data['CPU_ARCH'] = platform.architecture()[0]
data['CPU_COUNT'] = multiprocessing.cpu_count()

data['MEM_TOTAL'] = int(subprocess.check_output('grep MemTotal /proc/meminfo | awk \'{print $2}\'', shell=True))
data['MEM_FREE'] = int(subprocess.check_output('grep MemFree /proc/meminfo | awk \'{print $2}\'', shell=True))
data['MEM_USED'] = data['MEM_TOTAL'] - data['MEM_FREE']

data['PROCESS_COUNT'] = int(subprocess.check_output('ps -A --no-headers | wc -l', shell=True))

data['NET_INTERFACE'] = subprocess.check_output("ip route get 1| awk '{print $5;exit}'", shell=True).strip()
data['IP_ADDR'] = subprocess.check_output("ip route get 1 | awk '{print $NF;exit}'", shell=True).strip()
data['MAC_ADDR'] = subprocess.check_output("cat /sys/class/net/"+data['NET_INTERFACE']+"/address", shell=True).strip()

def CPU():
  return subprocess.check_output("top -b -n2 -d1 | grep 'Cpu(s)' | awk '{print $2 + $4}' | sed -n 2p", shell=True, stderr=None).strip()

def RX():
  RX1 = int(subprocess.check_output("cat /sys/class/net/"+data['NET_INTERFACE']+"/statistics/rx_bytes", shell=True))
  time.sleep(1)
  RX2 = int(subprocess.check_output("cat /sys/class/net/"+data['NET_INTERFACE']+"/statistics/rx_bytes", shell=True))
  return (RX2 - RX1)/1024.0

def TX():
  TX1 = int(subprocess.check_output("cat /sys/class/net/"+data['NET_INTERFACE']+"/statistics/tx_bytes", shell=True))
  time.sleep(1)
  TX2 = int(subprocess.check_output("cat /sys/class/net/"+data['NET_INTERFACE']+"/statistics/tx_bytes", shell=True))
  return (TX2 - TX1)/1024.0

print 'All memory values in kb'

for i in data:
  print i,':',data[i]

X = [i for i in xrange(60)]
Y = [0.0 for i in xrange(60)]


plt.ion()
graph = plt.plot(X,Y)[0]
axes = plt.gca()
axes.set_ylim([0,100])
while True:
  Y.pop(0)
  Y.append(CPU())
  print Y
  graph.set_ydata(Y)
  # axes.set_ylim([0,max(Y)*1.1])
  # axes.relim()
  plt.draw()